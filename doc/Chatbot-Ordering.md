# Chatbot Shopping/Ordering


Si consideri l’obiettivo di sviluppare un chatbot su piattaforma facebook messenger che abbia come finalità quella di permettere, a qualsiasi negozio al dettaglio, di poter vendere un piccolo gruppo di prodotti altamente disponibili (per consegna a domicilio o ritiro in loco) previo ordine online.

La consegna a domicilio è limitata a quei negozi che hanno il servizio col proprio fattorino.


## Flusso di interazione Logico

Il cliente, identificato all’ingresso del chatbot con un **messenger-user-id** univoco e persistente assegnato dalla **piattaforma facebook**, interagisce con il servizio e dopo una serie di informazioni selezionate (città e indirizzo) esegue le seguenti azioni:

1.	Si trova a dover premere il pulsante “Vedi Locali”. Tale azione apre una webview, dal basso verso l’alto, con la lista dei locali disponibili.

2.	L’utente seleziona un locale e viene reindirizzato, sempre all’interno della webview, nella **lista dei prodotti**.

3.	L’utente aggiunge diversi prodotti al carrello (la modalità di aggiunta verrà discussa in uno specifico capitolo) con relativa quantità. Il carrello si trova sempre nella webview ma in un'altra pagina. Puo essere visionato cliccando su “Vai al carrello” e si può tornare indietro alla lista dei prodotti con la freccia < (back). 

4.	Terminata la scelta dei prodotti, l’utente preme il pulsante **“Vai Carrello”** (se si trova ancora nella lista prodotti, e poi), preme **“Vai al checkout”**. La webview viene chiusa e il chatbot scrive sulla chat:

    a.	**Se è il primo ordine in assoluto dell’utente ->** significa che l’utente deve dare il consenso alla privacy. Il chatbot chiede mail, numero di telefono, note d’ordine e consenso alla privacy prima di dire “Perfetto, ecco il riepilogo dell’ordine”.

    b.	**Se l’utente ha già dato il consenso ->** Il chatbot dice direttamente “Perfetto, ecco il riepilogo dell’ordine”.

## Modalità di Aggiunta al Carrello

Proposta

![sequenza](https://bitbucket.org/reeturn/messenger-chatbot/raw/1a02f539d69323d4a9d9e0272a6c862fc3b87564/doc/assets/images/sequenza.jpg)

Altrimenti un suggerimento viene da Glovo, che non usa un popup ma dei “+” su ogni riga,…e se aggiungi il primo elemento compare il “-“ per toglierlo.
Scaricare Glovo app per dare un occhiata sul funzionamento di quel carrello.

Al posto della schermata centrale col pop-up potrebbe esserci questo caso qui.

![sequenza_alternativa](https://bitbucket.org/reeturn/messenger-chatbot/raw/b478e69fbd4e74eaa0f4194c6012e16d305ee794/doc/assets/images/sequenza_viewAltenativa.jpg)